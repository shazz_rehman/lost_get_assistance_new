import 'package:flutter/material.dart';
import 'package:lost_tracking/providers/auth_provider.dart';
import 'package:lost_tracking/providers/dio_client.dart';
import 'package:lost_tracking/services/http_service.dart';

import 'package:lost_tracking/services/navigation_service.dart';
import 'package:lost_tracking/utils/routes.dart';
import 'package:lost_tracking/utils/service_locator.dart';
import 'package:provider/provider.dart';

class AcceptEventInvite extends StatefulWidget {
  final String eventId;
  const AcceptEventInvite({Key? key, required this.eventId}) : super(key: key);

  @override
  _AcceptEventInviteState createState() => _AcceptEventInviteState();
}

class _AcceptEventInviteState extends State<AcceptEventInvite> {
  var navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<AuthProvider>(context, listen: false).user.id;
    HttpService httpService = HttpService();
    return Scaffold(
      body: Consumer<DioServices>(
        builder: (context, value, _) {
          return Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                InkWell(
                  onTap: () async {
                    print("tapped");
                    httpService.postAcceptEvent(
                      eventID: widget.eventId,
                      userID: user,
                      // 7LBnRc0rouhvvpke7DOfT1KfO4P2'
                    );
                    navigationService.navigateTo(HomeScreenRoute);

                    // value.acceptInvite().then(
                    //       (value) =>
                    //           navigationService.navigateTo(HomeScreenRoute),
                    //     );
                    // var storage = await SharedPreferences.getInstance();
                    // var deepLink = storage.get('deepLinkGroupId').toString();
                    // if (deepLink != null) {
                    //   var user=Provider.of<AuthProvider>(context,listen: false).user.id;
                    //   Provider.of<EventProvider>(context,listen: false).callPostAcceptEvent(deepLink, user);
                    //   storage.remove('deepLinkGroupId');
                    //   //var userData = await http!.updateUserData(user.uid.toString());
                    //   print("data link $deepLink");
                    //
                    // }
                  },
                  child: Text(
                    'Accept',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.green,
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    'Reject',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.red,
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
