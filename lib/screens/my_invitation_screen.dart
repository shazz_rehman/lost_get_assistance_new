import 'package:flutter/material.dart';
import 'package:lost_tracking/models/get_inviteUserList/get_event/get_event.dart';
import 'package:lost_tracking/models/get_inviteUserList/get_user_invite.dart';
import 'package:lost_tracking/providers/event_provider.dart';
import 'package:lost_tracking/services/http_service.dart';
import 'package:provider/provider.dart';

class MyInvitationScreen extends StatefulWidget {
  const MyInvitationScreen({Key? key}) : super(key: key);

  @override
  State<MyInvitationScreen> createState() => _MyInvitationScreenState();
}

class _MyInvitationScreenState extends State<MyInvitationScreen> {
  HttpService httpService = HttpService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        title: Text("My Invitations"),
        centerTitle: true,
      ),
      body: Consumer<EventProvider>(
        builder: (context, value, _) {
          return FutureBuilder<GetUserInvite>(
            future: value.getInvitesOfUser(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 8),
                      snapshot.data!.data!.totalCount == 0
                          // print(snapshot.data!.data!.totalCount.toString())
                          ? Text(
                              "No Data found",
                              style: TextStyle(color: Colors.black),
                            )
                          : SizedBox(
                              height: MediaQuery.of(context).size.height,
                              child: ListView.builder(
                                padding: EdgeInsets.only(bottom: 100),
                                scrollDirection: Axis.vertical,
                                itemBuilder: (BuildContext context, index) {
                                  final ListOfEvents _listOfEvents =
                                      snapshot.data!.data!.list[index];
                                  return AcceptInvationTile(
                                    listOfEvents: _listOfEvents,
                                  );
                                },
                                itemCount: snapshot.data!.data!.list.length,
                              ),
                            )

                      // SizedBox(
                      //   height: 200,
                      // )
                    ],
                  ),
                );
              } else
                return Center(
                  child: CircularProgressIndicator(),
                );
            },
          );
        },
      ),
    );
  }
}

class AcceptInvationTile extends StatefulWidget {
  final ListOfEvents listOfEvents;

  const AcceptInvationTile({Key? key, required this.listOfEvents})
      : super(key: key);

  @override
  State<AcceptInvationTile> createState() => _AcceptInvationTileState();
}

class _AcceptInvationTileState extends State<AcceptInvationTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 12,
        vertical: 4,
      ),
      margin: EdgeInsets.symmetric(
        horizontal: 12,
        vertical: 8,
      ),
      // height: 220,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16.0),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 4,
            spreadRadius: 2,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: Column(
        children: [
          Container(
            height: 160,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              image: DecorationImage(
                image: NetworkImage(
                  widget.listOfEvents.imageUrl ?? "",
                ),
                fit: BoxFit.cover,
              ),
            ),
            // child: Image.network(
            //   widget.listOfEvents.imageUrl ?? "",
            //   fit: BoxFit.cover,
            // ),
          ),
          SizedBox(height: 8),
          Row(
            children: [
              Text(
                "Invitation Type : ",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              Text(
                widget.listOfEvents.title ?? "",
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text(
                "Location : ",
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
              Text(
                widget.listOfEvents.location ?? "",
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text(
                "Event Date : ",
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
              Text(
                widget.listOfEvents.eventDate ?? "",
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
