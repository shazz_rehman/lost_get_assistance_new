import 'package:flutter/material.dart';
import 'package:flutter_contacts/flutter_contacts.dart';

import 'package:lost_tracking/providers/auth_provider.dart';
import 'package:lost_tracking/services/navigation_service.dart';
import 'package:lost_tracking/services/util_service.dart';
import 'package:lost_tracking/utils/routes.dart';
import 'package:lost_tracking/utils/service_locator.dart';
import 'package:provider/provider.dart';

class PhoneContactPicker extends StatefulWidget {
  const PhoneContactPicker({Key? key}) : super(key: key);

  @override
  _PhoneContactPickerState createState() => _PhoneContactPickerState();
}

class _PhoneContactPickerState extends State<PhoneContactPicker> {
  List<Contact>? _contacts;
  bool _permissionDenied = false;

  @override
  void initState() {
    super.initState();
    _fetchContacts();
  }

  Future _fetchContacts() async {
    if (!await FlutterContacts.requestPermission(readonly: true)) {
      setState(() => _permissionDenied = true);
    } else {
      final contacts = await FlutterContacts.getContacts();
      setState(() => _contacts = contacts);
    }
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xFF051b55),
            centerTitle: true,
            title: const Text('My Contacts'),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: _body(),
        ),
      );

  Widget _body() {
    if (_permissionDenied) {
      return const Center(
        child: Text('Permission denied'),
      );
    }
    if (_contacts == null) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }
    return ListView.builder(
      padding: EdgeInsets.symmetric(vertical: 20),
      itemCount: _contacts!.length,
      itemBuilder: (context, i) => ListTile(
        title: Text(_contacts![i].displayName),
        onTap: () async {
          final fullContact =
              await FlutterContacts.getContact(_contacts![i].id);
          await Navigator.of(context).push(
            MaterialPageRoute(
              builder: (_) => ContactPage(fullContact!),
            ),
          );
        },
      ),
    );
  }
}

class ContactPage extends StatefulWidget {
  final Contact contact;
  const ContactPage(this.contact, {Key? key}) : super(key: key);

  @override
  State<ContactPage> createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  var navigationService = locator<NavigationService>();
  List<String> people = [];
  UtilService? utilService = locator<UtilService>();
  // late TextEditingController _controllerPeople, _controllerMessage;
  String selectedNumber = '';

  late TextEditingController nameController;
  late TextEditingController phoneController;
  late TextEditingController relation;
  late TextEditingController address;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(text: widget.contact.name.first);
    phoneController = TextEditingController(
      text: widget.contact.phones.isNotEmpty
          ? widget.contact.phones.first.number
          : "No Number Found",
    );
    relation = TextEditingController(text: "Friend");
    address = TextEditingController(text: "None");
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF051b55),
          title: Text("Contact Details"),
          centerTitle: true,
        ),
        body: Consumer<AuthProvider>(
          builder: (context, value, _) {
            return Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: InkWell(
                      onTap: () {  
                         
                        value
                            .addUserContact(
                              nameController.text,
                              phoneController.text,
                              address.text,
                              relation.text,
                            )
                            .then(
                              (value) => Provider.of<AuthProvider>(
                                context,
                                listen: false,
                              ).getAllContacts,
                            );
                        navigationService.navigateTo(SendMessageScreenRoute);
                      },
                      splashColor: Colors.transparent,   
                      child: SizedBox(
                        height: 59,
                        width: 330,
                        child: Material(
                          borderRadius: BorderRadius.circular(24.0),
                          color: Colors.red,
                          child: const Center(
                            child: Text(
                              "Add to My Contacts",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(20),
                  margin: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16.0),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Contact Card",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 22,
                                    color: Colors.teal),
                              ),
                              SizedBox(height: 15),
                              TextFormField(
                                controller: nameController,
                                decoration: const InputDecoration(
                                  label: Text("Name"),
                                  border: OutlineInputBorder(),
                                ),
                              ),
                              SizedBox(height: 15),
                              // Text(widget.contact.name.last),
                              TextFormField(
                                controller: relation,
                                decoration: const InputDecoration(
                                  label: Text("Relation"),
                                  border: OutlineInputBorder(),
                                ),
                              ),
                              SizedBox(height: 15),
                              TextFormField(
                                controller: phoneController,
                                decoration: const InputDecoration(
                                  label: Text("Phone Number"),
                                  border: OutlineInputBorder(),
                                ),
                              ),
                              SizedBox(height: 15),
                              TextFormField(
                                controller: address,
                                decoration: const InputDecoration(
                                  label: Text("Addressr"),
                                  border: OutlineInputBorder(),
                                ),
                              ),

                              SizedBox(height: 50),

                              // Row(
                              //   children: [
                              //     Expanded(
                              //       child: CupertinoButton.filled(
                              //         child: Text("Add to My Contacts"),
                              //         onPressed: () {
                              //           value
                              //               .addUserContact(
                              //                 nameController.text,
                              //                 phoneController.text,
                              //                 address.text,
                              //                 relation.text,
                              //               )
                              //               .then(
                              //                 (value) =>
                              //                     Provider.of<AuthProvider>(
                              //                             context,
                              //                             listen: false)
                              //                         .getAllContacts,
                              //               );
                              //           Navigator.pushReplacement(
                              //             context,
                              //             MaterialPageRoute(
                              //               builder: (context) =>
                              //                   SendMessageScreen(),
                              //             ),
                              //           );
                              //           value.notifyListeners();
                              //         },
                              //       ),
                              //     ),
                              //   ],
                              // )
                            ],
                          ),
                        ),
                        //
                      ],
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      );
}

// InkWell(
//   onTap: () {
//     // Navigator.popAndPushNamed(context, HomeScreen.route);
//   },
//   splashColor: Colors.transparent,
//   child: SizedBox(
//     height: 59,
//     width: 330,
//     child: Material(
//       borderRadius: BorderRadius.circular(24.0),
//       color: Colors.red,
//       child: const Center(
//         child: Text(
//           "Login",
//           style: TextStyle(
//             color: Colors.white,
//             fontSize: 16,
//           ),
//         ),
//       ),
//     ),
//   ),
// ),
