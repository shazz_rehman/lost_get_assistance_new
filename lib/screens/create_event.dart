import 'dart:io';

import 'package:dotted_border/dotted_border.dart';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lost_tracking/models/user.dart';
import 'package:lost_tracking/providers/auth_provider.dart';
import 'package:lost_tracking/providers/event_provider.dart';
import 'package:lost_tracking/services/navigation_service.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:lost_tracking/services/util_service.dart';
import 'package:lost_tracking/utils/routes.dart';
import 'package:lost_tracking/utils/service_locator.dart';
import 'package:intl/intl.dart';
import 'package:lost_tracking/widget/column_scroll_view.dart';
import 'package:provider/provider.dart';

class CreateEvent extends StatefulWidget {
  const CreateEvent({Key? key}) : super(key: key);

  @override
  _CreateEventState createState() => _CreateEventState();
}

class _CreateEventState extends State<CreateEvent> {
  var navigationService = locator<NavigationService>();
  FocusNode focusNode1 = FocusNode();
  //final _location=TextEditingController();
  final titleCnt = TextEditingController();
  final locationCnt = TextEditingController();
  final dateCnt = TextEditingController();
  final startTimeCnt = TextEditingController();
  final endTimeCnt = TextEditingController();

  final ImagePicker _picker = ImagePicker();

  XFile? _image;
  Future getImagefromcamera() async {
    var image = await _picker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = image;

      print(_image!.path);
    });
  }

  Future getImagefromGallery() async {
    var image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;

      print('file path----${_image!.path}');
    });
  }

  // var uuid = new Uuid();
  // String? _sessionToken;
  // List<dynamic> _placeList = [];
  //
  // _onChanged(String abc) {
  //   if (_sessionToken == null) {
  //     setState(() {
  //       _sessionToken = uuid.v4();
  //     });
  //   }
  //   getSuggestion(abc);
  // }

  // void getSuggestion(String input) async {
  //   String aPiKey = "";
  //   // String type = '(regions)';
  //   String baseURL =
  //       'https://maps.googleapis.com/maps/api/place/autocomplete/json';
  //   String request =
  //       '$baseURL?input=$input&key=$aPiKey&sessiontoken=$_sessionToken&region=pk';
  //   var response = await http.get(Uri.parse(request));
  //   if (response.statusCode == 200) {
  //     setState(() {
  //       _placeList = json.decode(response.body)['predictions'];
  //     });
  //   } else {
  //     throw Exception('Failed to load predictions');
  //   }
  // }
  bool isLoadingProgress = false;

  @override
  void dispose() {
    focusNode1.dispose();
    super.dispose();
  }

  // String imgUrl="";
  // String title="";
  // String location="";
  // String date="";
  // String startTime="";
  // String endTime="";

  final dateFormat = DateFormat.yMMMd();
  final timeFormat = DateFormat("hh:mm a");

  final utilService = locator<UtilService>();

  @override
  Widget build(BuildContext context) {
    double heights = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70.0),
        child: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            iconSize: 25,
            icon: Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
            onPressed: () {
              navigationService.closeScreen();
              //navigationService.navigateTo(HomeScreenRoute);
            },
          ),
          title: Text(
            "Create Event",
            style: TextStyle(
              fontWeight: FontWeight.w400,
              color: Colors.black,
            ),
          ),
          centerTitle: true,
        ),
      ),
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.all(10),
            child: ColumnScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      DottedBorder(
                          dashPattern: [
                            4,
                            4,
                          ],
                          color: Colors.grey[400]!,
                          borderType: BorderType.RRect,
                          radius: Radius.circular(9),
                          //padding: EdgeInsets.all(8),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            child: _image != null
                                ? Container(
                                    height: heights * 0.3,
                                    width: width,
                                    child: Image.file(
                                      File(_image!.path),
                                      // width: 90,
                                      // height: 90,
                                      fit: BoxFit.cover,
                                    ),
                                  )
                                : Container(
                                    height: heights * 0.2,
                                    width: width,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        InkWell(
                                          onTap: () {
                                            _showPicker(context);
                                          },
                                          child: Container(
                                            height: 35,
                                            width: 35,
                                            decoration: BoxDecoration(
                                                color: Colors.red,
                                                borderRadius:
                                                    BorderRadius.circular(6)),
                                            child: Center(
                                              child: Icon(
                                                Icons.add,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Text('Add cover image for your event')
                                      ],
                                    ),
                                  ),
                          )),

                      TextField(
                        focusNode: focusNode1,
                        cursorColor: Colors.red,
                        controller: titleCnt,
                        decoration: InputDecoration(
                          //suffixIcon: Icon(Icons.arrow_forward_ios,color: focusNode1.hasFocus? Colors.red:Colors.grey,),

                          focusColor: Colors.red,
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.red),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey[300]!),
                          ),
                          labelText: "Event Title",
                          labelStyle:
                              TextStyle(color: Colors.red, fontSize: 13),
                          //hintStyle: TextStyle(fontWeight: FontWeight.w300, color: Colors.red)
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),

                      TextField(
                        // onChanged: (e){
                        //
                        //   //_onChanged(_location.text);
                        //   setState(() {});
                        // },
                        controller: locationCnt,

                        cursorColor: Colors.red,
                        decoration: InputDecoration(
                          suffixIcon: Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.grey,
                            size: 14,
                          ),

                          focusColor: Colors.red,
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.red),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey[300]!),
                          ),
                          labelText: "Location",
                          labelStyle:
                              TextStyle(color: Colors.red, fontSize: 14),
                          //hintStyle: TextStyle(fontWeight: FontWeight.w300, color: Colors.red)
                        ),
                      ),
                      //SizedBox(height: 10,),
                      dateField(),

                      // BasicDateTimeField(),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          timeField("Start Time"),
                          timeField("End Time")
                          // TimeField(label: "Start Time",),
                          // TimeField(label: "End Time",)
                        ],
                      ),
                    ],
                  ),
                  Container(
                    height: 50,
                    width: width,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(32)),
                    child: TextButton(
                      onPressed: () {
                        // print('title: ${titleCnt.text}');
                        // print('location: ${locationCnt.text}');
                        // print('date: ${dateCnt.text}');
                        //print('start: ${startTimeCnt.text}');
                        //print('end: ${endTimeCnt.text}');
                        // print('end: ${_image!.path}');

                        //var format = DateFormat("HH:mm");

                        var one = timeFormat.parse("${startTimeCnt.text}");
                        var two = timeFormat.parse("${endTimeCnt.text}");
                        print('start: $one');
                        print('end: $two');

                        print(
                            "this is the difference${two.difference(one).inMinutes}");
                        // print("this is the date${DateTime.now().difference(date)}");

                        AppUser user =
                            Provider.of<AuthProvider>(context, listen: false)
                                .user;

                        // EventModel event =
                        //     Provider.of(context, listen: false);

                        if (_image == null ||
                            titleCnt.text.isEmpty ||
                            locationCnt.text.isEmpty ||
                            dateCnt.text.isEmpty ||
                            startTimeCnt.text.isEmpty ||
                            endTimeCnt.text.isEmpty) {
                          utilService.showToast("All Fields are required");
                          return;
                        } else {
                          if (two.difference(one).inMinutes < 1) {
                            utilService.showToast(
                                "Start time can't be less than or equal to end time");
                          } else {
                            setState(() {
                              isLoadingProgress = true;
                            });

                            Provider.of<EventProvider>(context, listen: false)
                                .uploadImage(user.id!, File(_image!.path))
                                .then(
                              (value) {
                                Map<String, dynamic> data = {
                                  "imageUrl": "$value",
                                  "title": "${titleCnt.text}",
                                  "location": "${locationCnt.text}",
                                  "eventDate": "${dateCnt.text}",
                                  "startTime": "${startTimeCnt.text}",
                                  "endTime": "${endTimeCnt.text}"
                                };

                                Provider.of<EventProvider>(context,
                                        listen: false)
                                    .callPostEvent(data)
                                    .then(
                                  (value1) {
                                    if (value == "fail") {
                                      setState(() {
                                        isLoadingProgress = false;
                                      });
                                      utilService
                                          .showToast("Fail to create event");
                                    } else {
                                      setState(() {
                                        isLoadingProgress = false;
                                      });
                                      navigationService.closeScreen();
                                      navigationService.navigateTo(
                                        MyEventScreenRoute,
                                      );
                                    }
                                  },
                                );
                              },
                            );
                          }
                        }
                      },
                      child: Text(
                        'Continue',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          if (isLoadingProgress)
            Positioned.fill(
              child: Align(
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              ),
            ),
        ],
      ),
    );
  }

  void _showPicker(context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('Photo Library'),
                    onTap: () {
                      getImagefromGallery();
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.photo_camera),
                  title: new Text('Camera'),
                  onTap: () {
                    getImagefromcamera();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget timeField(String label) {
    return Container(
      width: 150,
      child: DateTimeField(
        controller: label == "Start Time" ? startTimeCnt : endTimeCnt,
        decoration: InputDecoration(
          // contentPadding:
          // const EdgeInsets.symmetric(vertical: 12.0, horizontal: 15),
          suffixIcon: Icon(
            Icons.access_time,
            size: 15,
          ),

          //hintText: '25-03-21',
          focusColor: Colors.red,
          labelText: "$label",
          labelStyle: TextStyle(
            color: Colors.red,
            fontSize: 14,
          ),
          //hintStyle: TextStyle(color: Colors.grey, fontSize: 10),

          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]!),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide:
                BorderSide(color: Theme.of(context).indicatorColor, width: 2),
          ),
        ),
        format: timeFormat,
        onShowPicker: (context, currentValue) async {
          final TimeOfDay? time = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
          );

          //print('abc1${time!.format(context)}');
          //print('abc2${time.hour}:${time.minute}:${time.period}:${time.format(context)}:${time.hourOfPeriod}');
          // print('abc3${TimeOfDay.fromDateTime(time)}');
          // if (widget.start!) {
          //   var format = DateFormat("HH:mm");
          //   var one = format.parse("10:40");
          //   var two = format.parse("18:20");
          //   print("${two.difference(one).inMinutes}");
          //   print("start${time!.hour}:${time.minute}");
          //
          //   // Provider.of<CreateEventProvider>(context, listen: false).startTime24hr = "${time.hour}:${time.minute}";
          //   //
          //   // Provider.of<CreateEventProvider>(context, listen: false).startTime = time.format(context);
          // } else {
          //   // print("end${time!.hour}:${time.minute}");
          //   // Provider.of<CreateEventProvider>(context, listen: false)
          //   //     .endTime24hr = "${time.hour}:${time.minute}";
          //   // Provider.of<CreateEventProvider>(context, listen: false)
          //   //     .endTime = time.format(context);
          // }

          return time == null ? null : DateTimeField.convert(time);
        },
      ),
    );
  }

  Widget dateField() {
    return DateTimeField(
      cursorColor: Colors.red,
      controller: dateCnt,
      decoration: InputDecoration(
        // contentPadding:
        // const EdgeInsets.symmetric(vertical: 12.0, horizontal: 15),
        suffixIcon: Icon(
          Icons.calendar_today_outlined,
          size: 15,
        ),
        //hintText: '25-03-21',
        focusColor: Colors.red,
        labelText: "Date",
        labelStyle: TextStyle(color: Colors.red, fontSize: 14),
        //hintStyle: TextStyle(color: Colors.grey, fontSize: 10),

        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.grey[300]!),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          borderSide:
              BorderSide(color: Theme.of(context).indicatorColor, width: 2),
        ),
      ),
      format: dateFormat,
      onShowPicker: (context, currentValue) async {
        final date = await showDatePicker(
            context: context,
            firstDate: DateTime.now(),
            initialDate: currentValue ?? DateTime.now(),
            lastDate: DateTime(2100));
        if (date != null) {
          // Provider.of<CreateEventProvider>(context, listen: false).date =
          //     date;
          // final time = await showTimePicker(
          //   context: context,
          //   initialTime:
          //       TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
          // );
          return date; //DateTimeField.combine(date);
        } else {
          return currentValue;
        }
      },
    );
  }
}


// class BasicDateTimeField extends StatelessWidget {
//   //final format = DateFormat("dd-MM-yyyy");
//   final format = DateFormat.yMMMd();
//   @override
//   Widget build(BuildContext context) {
//
//       return DateTimeField(
//         cursorColor: Colors.red,
//
//         decoration: InputDecoration(
//
//
//           // contentPadding:
//           // const EdgeInsets.symmetric(vertical: 12.0, horizontal: 15),
//           suffixIcon: Icon(Icons.calendar_today_outlined,size: 15,),
//           //hintText: '25-03-21',
//           focusColor: Colors.red,
//           labelText: "Date",
//           labelStyle: TextStyle(color: Colors.red, fontSize: 14),
//           //hintStyle: TextStyle(color: Colors.grey, fontSize: 10),
//
//           enabledBorder: UnderlineInputBorder(
//             borderSide: BorderSide(color: Colors.grey[300]!),
//           ),
//           focusedBorder: OutlineInputBorder(
//             borderRadius: BorderRadius.all(Radius.circular(5)),
//             borderSide: BorderSide(
//                 color: Theme.of(context).indicatorColor, width: 2),
//           ),
//         ),
//         format: format,
//         onShowPicker: (context, currentValue) async {
//           final date = await showDatePicker(
//               context: context,
//               firstDate: DateTime(1900),
//               initialDate: currentValue ?? DateTime.now(),
//               lastDate: DateTime(2100));
//           if (date != null) {
//             // Provider.of<CreateEventProvider>(context, listen: false).date =
//             //     date;
//             // final time = await showTimePicker(
//             //   context: context,
//             //   initialTime:
//             //       TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
//             // );
//             return date; //DateTimeField.combine(date);
//           } else {
//             return currentValue;
//           }
//         },
//       );
//
//   }
// }



// class TimeField extends StatelessWidget {
//   final String? label;
//    TimeField({Key? key,this.label}) : super(key: key);
//
//   final format = DateFormat("hh:mm a");
//
//   @override
//   Widget build(BuildContext context) {
//
//     return Container(
//       width: 150,
//       child: DateTimeField(
//
//         decoration: InputDecoration(
//
//
//
//
//           // contentPadding:
//           // const EdgeInsets.symmetric(vertical: 12.0, horizontal: 15),
//           suffixIcon: Icon(Icons.access_time,size: 15,),
//
//           //hintText: '25-03-21',
//           focusColor: Colors.red,
//           labelText: "$label",
//           labelStyle: TextStyle(color: Colors.red, fontSize: 14,),
//           //hintStyle: TextStyle(color: Colors.grey, fontSize: 10),
//
//           enabledBorder: UnderlineInputBorder(
//             borderSide: BorderSide(color: Colors.grey[300]!),
//           ),
//           focusedBorder: OutlineInputBorder(
//             borderRadius: BorderRadius.all(Radius.circular(5)),
//             borderSide: BorderSide(
//                 color: Theme.of(context).indicatorColor, width: 2),
//           ),
//         ),
//         format: format,
//         onShowPicker: (context, currentValue) async {
//           final TimeOfDay? time = await showTimePicker(
//             context: context,
//             initialTime:
//             TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
//           );
//
//           //print('abc1${time!.format(context)}');
//           //print('abc2${time.hour}:${time.minute}:${time.period}:${time.format(context)}:${time.hourOfPeriod}');
//           // print('abc3${TimeOfDay.fromDateTime(time)}');
//           // if (widget.start!) {
//           //   var format = DateFormat("HH:mm");
//           //   var one = format.parse("10:40");
//           //   var two = format.parse("18:20");
//           //   print("${two.difference(one).inMinutes}");
//           //   print("start${time!.hour}:${time.minute}");
//           //
//           //   // Provider.of<CreateEventProvider>(context, listen: false).startTime24hr = "${time.hour}:${time.minute}";
//           //   //
//           //   // Provider.of<CreateEventProvider>(context, listen: false).startTime = time.format(context);
//           // } else {
//           //   // print("end${time!.hour}:${time.minute}");
//           //   // Provider.of<CreateEventProvider>(context, listen: false)
//           //   //     .endTime24hr = "${time.hour}:${time.minute}";
//           //   // Provider.of<CreateEventProvider>(context, listen: false)
//           //   //     .endTime = time.format(context);
//           // }
//
//           return time == null ? null : DateTimeField.convert(time);
//         },
//       ),
//     );
//
//   }
// }


