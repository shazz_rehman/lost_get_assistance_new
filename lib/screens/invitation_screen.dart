import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:lost_tracking/screens/dynamic_links.dart';
import 'package:lost_tracking/services/navigation_service.dart';
import 'package:lost_tracking/utils/service_locator.dart';

class InvitationScreen extends StatefulWidget {
  const InvitationScreen({Key? key}) : super(key: key);

  @override
  _InvitationScreenState createState() => _InvitationScreenState();
}

class _InvitationScreenState extends State<InvitationScreen> {
  var navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    double heights = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color(0xFF051b55),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70.0),
        child: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            iconSize: 25,
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              navigationService.closeScreen();
              //navigationService.navigateTo(HomeScreenRoute);
            },
          ),
          title: Text(
            "Create Event",
            style: TextStyle(fontWeight: FontWeight.w400, color: Colors.white),
          ),
        ),
      ),
      body: Container(
        height: heights * 0.9,
        width: width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(22),
            topRight: Radius.circular(22),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(),
        ),
      ),
    );
  }
}

Widget eventTile(
  context, {
  required String imgUrl,
  required String day,
  required String month,
  required String eventTitle,
  required String startTime,
  required String endTime,
  required String address,
  required String eventID,
}) {
  double heights = MediaQuery.of(context).size.height;
  double width = MediaQuery.of(context).size.width;
  return Container(
    margin: EdgeInsets.symmetric(vertical: 8),
    // height: heights * 0.33,
    width: width,
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            spreadRadius: 4,
            blurRadius: 4,
            offset: Offset(0, 3),
          )
        ]),
    child: Padding(
      padding: const EdgeInsets.all(4.0),
      child: Column(
        children: [
          Container(
            height: heights * 0.23,
            width: width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                child: CachedNetworkImage(
                  imageUrl: imgUrl,
                  placeholder: (context, url) => Image.asset(
                    "assets/images/placeholder.png",
                  ),
                  fit: BoxFit.cover,
                )
                // Image.network(
                //   imgUrl,
                //   //"assets/images/dummy_girl7.jpg",
                //   // width: 90,
                //   // height: 90,
                //   fit: BoxFit.cover,
                // ),
                ),
          ),
          SizedBox(height: 4),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    // onTap: () async {
                    //   var dynamicLinks = locator<DynamicLinkService>();
                    //   String url =
                    //       await dynamicLinks.getDynamicLinkUrl(eventID);
                    //   print(url);

                    //   await FlutterShare.share(
                    //     title: 'Example share',
                    //     text: url,
                    //     //filePath: docs[0] as String,
                    //   );
                    // },
                    child: Row(
                      children: [
                        Container(
                            constraints: BoxConstraints(
                              maxWidth: width * 0.55,
                              minWidth: 10,
                            ),
                            child: Text(
                              "$eventTitle ",
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: TextStyle(fontSize: 13),
                            )),
                        Text(
                          "(Family)",
                          style: TextStyle(fontSize: 13, color: Colors.red),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.access_time,
                            size: 11,
                            color: Colors.grey[400],
                          ),
                          Text(
                            ' $startTime to $endTime',
                            style: TextStyle(
                              color: Colors.grey[400],
                              fontSize: 11,
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            size: 11,
                            color: Colors.grey[400],
                          ),
                          Container(
                            //height: 15,
                            // width: width * 0.35,
                            // color: Colors.red,
                            child: Text(
                              ' $address',
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: TextStyle(
                                color: Colors.grey[400],
                                fontSize: 11,
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  )
                ],
              ),
              SizedBox(height: 4),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                    // height: 20,
                    // width: 20,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Colors.amber,
                    ),
                    child: InkWell(
                      onTap: () async {
                        var dynamicLinks = locator<DynamicLinkService>();
                        String url =
                            await dynamicLinks.getDynamicLinkUrl(eventID);
                        print("URL IS " + url);

                        await FlutterShare.share(
                          text: url,
                          title: 'Share',
                        );
                      },
                      child: Text("Share"),
                    ),
                  ),
                  SizedBox(width: 8),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    // height: 45,
                    // width: 45,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Center(
                      child: Column(
                        children: [
                          Text(
                            "$day",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 16),
                          ),
                          Text(
                            "$month",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    ),
  );
}
