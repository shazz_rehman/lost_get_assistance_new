import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:lost_tracking/screens/accept_event.dart';
import 'package:lost_tracking/services/http_service.dart';
import 'package:lost_tracking/services/storage_service.dart';
import 'package:lost_tracking/services/util_service.dart';
import 'package:lost_tracking/utils/service_locator.dart';

class DynamicLinkService {
  FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;
  HttpService? http = locator<HttpService>();
  var storageService = locator<StorageService>();
  UtilService util = locator<UtilService>();
// generate the short link of the app and send it to the user
  Future<String> getDynamicLinkUrl(String groupId) async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      // The Dynamic Link URI domain. You can view created URIs on your Firebase console
      uriPrefix: 'https://lostapp.page.link',
      // The deep Link passed to your application which you can use to affect change
      link: Uri.parse('https://lostapp.com/$groupId'),
      // Android application details needed for opening correct app on device/Play Store
      androidParameters: AndroidParameters(
        packageName: 'com.appstirr.lost',
        minimumVersion: 0,
      ),
      // // iOS application details needed for opening correct app on device/App Store
      iosParameters: IOSParameters(
        bundleId: 'com.appstirr.lostFlutter',
        minimumVersion: '0',
      ),
    );
    final ShortDynamicLink shortDynamicLink =
        await dynamicLinks.buildShortLink(parameters);
    final Uri uri = shortDynamicLink.shortUrl;
    return uri.toString();
  }

  Future handleDynamicLinks(BuildContext context) async {
    final PendingDynamicLinkData? data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    handleDeepLink(data);
    //for ground dynamic link logic
    FirebaseDynamicLinks.instance.onLink.listen(
      (dynamicLinkData) async {
        var user = FirebaseAuth.instance.currentUser;
        //Navigator.pushNamed(context, "${dynamicLinkData.link.pathSegments[0]}");
        print("data from dynamic--${dynamicLinkData.link.pathSegments[0]}");

        // var storage = await SharedPreferences.getInstance();
        // var deepLink = storage.get('deepLinkGroupId').toString();
        //if(deepLink!="null"){
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => AcceptEventInvite(eventId: dynamicLinkData.link.pathSegments[0],),
          ),
        );
        //}

        // var storage = await SharedPreferences.getInstance();
        // await storage.setString('deepLinkGroupId', dynamicLinkData.link.pathSegments[0].toString());
      },
    ).onError(
      (error) {
        print(error);
      },
    );
  }

  void handleDeepLink(PendingDynamicLinkData? data) {
    final Uri? deepLink = data?.link;
    if (deepLink != null) {
      print('Deep link found: $deepLink');
    }
  }
}
