import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:lost_tracking/providers/event_provider.dart';
import 'package:lost_tracking/screens/invitation_screen.dart';
import 'package:lost_tracking/services/navigation_service.dart';
import 'package:lost_tracking/services/util_service.dart';
import 'package:lost_tracking/utils/connectivity_status.dart';
import 'package:lost_tracking/utils/service_locator.dart';
import 'package:provider/provider.dart';

class MyEventScreen extends StatefulWidget {
  const MyEventScreen({Key? key}) : super(key: key);

  @override
  _MyEventScreenState createState() => _MyEventScreenState();
}

class _MyEventScreenState extends State<MyEventScreen> {
  var navigationService = locator<NavigationService>();
  var singleton = locator<ConnectionStatusSingleton>();
  final DateFormat startformatterDate = DateFormat('d');
  UtilService util = locator<UtilService>();

  bool? _showLoader;

  getData() async {
    bool abc = await singleton.checkConnection();
    if (abc) {
      Provider.of<EventProvider>(context, listen: false).eventData.clear();
      Provider.of<EventProvider>(context, listen: false)
          .callGetEvent()
          .then((value) {
        if (value == "fail") {
          setState(() {
            _showLoader = true;
          });
        }
      });
    } else {
      util.showToast("no internet");
    }
  }

  @override
  void initState() {
    getData();

    super.initState();
  }
  // Future<void> shareFile() async {
  //   //List<dynamic> docs = await DocumentsPicker.pickDocuments;
  //   //if (docs == null || docs.isEmpty) return null;
  //
  //   await FlutterShare.share(
  //     title: 'Example share',
  //     text: 'Example share text',
  //     //filePath: docs[0] as String,
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    double heights = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color(0xFF051b55),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70.0),
        child: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            iconSize: 25,
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              navigationService.closeScreen();
              //navigationService.navigateTo(HomeScreenRoute);
            },
          ),
          title: Text(
            "My Events",
            style: TextStyle(
              fontWeight: FontWeight.w400,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
        ),
      ),
      body: Consumer<EventProvider>(
        builder: (context, i, _) {
          print(i.eventData.length);
          return _showLoader == null
              ? i.eventData.isNotEmpty
                  ? Container(
                      height: heights * 0.9,
                      width: width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(22),
                          topRight: Radius.circular(22),
                        ),
                      ),
                      child: ListView.builder(
                        padding: EdgeInsets.symmetric(
                          horizontal: 12.0,
                          vertical: 20,
                        ),
                        itemCount: i.eventData.length,
                        itemBuilder: (context, index) {
                          var parts = i.eventData[index].eventDate.split(' ');
                          var month = parts[0].trim();
                          var prefix = parts[1].split(',');
                          var day = prefix[0].trim();
                          print("day: $day");
                          print("month: $month");
                          return eventTile(context,
                              imgUrl: i.eventData[index].imageUrl,
                              day: "$day",
                              endTime: i.eventData[index].endTime,
                              startTime: i.eventData[index].startTime,
                              address: i.eventData[index].location,
                              eventTitle: i.eventData[index].title,
                              month: "$month",
                              eventID: i.eventData[index].id);
                        },
                      ),
                    )
                  : Center(
                      child: Text(
                        "No Events Found",
                        style: TextStyle(color: Colors.white),
                      ),
                    )
              : Center(
                  child: CircularProgressIndicator(
                    color: Colors.blue,
                  ),
                );
        },
      ),
    );
  }
}
