import 'package:json_annotation/json_annotation.dart';

part 'event_model.g.dart';

// ignore: deprecated_member_use
@JsonSerializable(nullable: false)
class EventModel {
  EventModel({
    required this.id,
    required this.createdOnDate,
    required this.imageUrl,
    required this.title,
    required this.userId,
    required this.location,
    required this.eventDate,
    required this.startTime,
    required this.endTime,
  });

  String id;
  int createdOnDate;
  String imageUrl;
  String title;
  String userId;
  String location;
  String eventDate;
  String startTime;
  String endTime;

  factory EventModel.fromJson(Map<String, dynamic> json) =>
      _$EventModelFromJson(json);

  Map<String, dynamic> toJson() => _$EventModelToJson(this);
  // factory EventModel.fromJson(Map<String, dynamic> json) => _fromJsonEventModelData(json);
  // Map<String, dynamic> toJson() => _toJsonEventModelData(this);

}

class Location {
  Location({
    required this.country,
    required this.long,
    required this.lat,
    required this.city,
    required this.address,
  });

  String country;
  int long;
  int lat;
  String city;
  String address;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        country: json["country"],
        long: json["long"],
        lat: json["lat"],
        city: json["city"],
        address: json["address"],
      );

  Map<String, dynamic> toJson() => {
        "country": country,
        "long": long,
        "lat": lat,
        "city": city,
        "address": address,
      };
}
