// To parse this JSON data, do
//
//     final acceptInvite = acceptInviteFromJson(jsonString);

// ignore_for_file: unnecessary_null_comparison

import 'dart:convert';

AcceptInvite acceptInviteFromJson(String str) =>
    AcceptInvite.fromJson(json.decode(str));

String acceptInviteToJson(AcceptInvite data) => json.encode(data.toJson());

class AcceptInvite {
  AcceptInvite({
    required this.isSuccess,
    required this.data,
    required this.isServerError,
    required this.message,
  });

  final bool? isSuccess;
  final bool? data;
  final bool? isServerError;
  final String? message;

  factory AcceptInvite.fromJson(Map<String, dynamic> json) => AcceptInvite(
        isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
        data: json["data"] == null ? null : json["data"],
        isServerError:
            json["isServerError"] == null ? null : json["isServerError"],
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "isSuccess": isSuccess == null ? null : isSuccess,
        "data": data == null ? null : data,
        "isServerError": isServerError == null ? null : isServerError,
        "message": message == null ? null : message,
      };
}
