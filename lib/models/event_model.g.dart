// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventModel _$EventModelFromJson(Map<String, dynamic> json) {
  return EventModel(
    id: json['id'] as String,
    createdOnDate: json['createdOnDate'] as int,
    imageUrl: json['imageUrl'] as String,
    title: json['title'] as String,
    userId: json['userId'] as String,
    location: json['location'] as String,
    eventDate: json['eventDate'] as String,
    startTime: json['startTime'] as String,
    endTime: json['endTime'] as String,
  );
}

Map<String, dynamic> _$EventModelToJson(EventModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdOnDate': instance.createdOnDate,
      'imageUrl': instance.imageUrl,
      'title': instance.title,
      'userId': instance.userId,
      'location': instance.location,
      'eventDate': instance.eventDate,
      'startTime': instance.startTime,
      'endTime': instance.endTime,
    };
