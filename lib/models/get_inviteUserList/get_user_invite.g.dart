// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_user_invite.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetUserInvite _$GetUserInviteFromJson(Map<String, dynamic> json) {
  return GetUserInvite(
    isSucess: json['isSucess'] as bool?,
    data: json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
    isServerError: json['isServerError'] as bool?,
    message: json['message'] as String?,
  );
}

Map<String, dynamic> _$GetUserInviteToJson(GetUserInvite instance) =>
    <String, dynamic>{
      'isSucess': instance.isSucess,
      'data': instance.data?.toJson(),
      'isServerError': instance.isServerError,
      'message': instance.message,
    };
