import 'package:lost_tracking/models/get_inviteUserList/get_event/get_event.dart';
import 'package:json_annotation/json_annotation.dart';
part 'get_invite_events.g.dart';


@JsonSerializable(explicitToJson: true)
class Data {
  Data({
    required this.list,
    required this.page,
    required this.size,
    required this.totalCount,
  });
  final List<ListOfEvents> list;
  final int? page;
  final int? size;
  final int? totalCount;

   factory Data.fromJson(Map<String, dynamic> json) =>
      _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}
