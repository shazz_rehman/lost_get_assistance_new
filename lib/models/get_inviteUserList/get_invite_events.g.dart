// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_invite_events.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    list: (json['list'] as List<dynamic>)
        .map((e) => ListOfEvents.fromJson(e as Map<String, dynamic>))
        .toList(),
    page: json['page'] as int?,
    size: json['size'] as int?,
    totalCount: json['totalCount'] as int?,
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'list': instance.list.map((e) => e.toJson()).toList(),
      'page': instance.page,
      'size': instance.size,
      'totalCount': instance.totalCount,
    };
