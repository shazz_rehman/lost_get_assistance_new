// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListOfEvents _$ListOfEventsFromJson(Map<String, dynamic> json) {
  return ListOfEvents(
    id: json['id'] as String?,
    createdOnDate: json['createdOnDate'] as int?,
    imageUrl: json['imageUrl'] as String?,
    title: json['title'] as String?,
    userId: json['userId'] as String?,
    location: json['location'] as String?,
    eventDate: json['eventDate'] as String?,
    startTime: json['startTime'] as String?,
    endTime: json['endTime'] as String?,
  );
}

Map<String, dynamic> _$ListOfEventsToJson(ListOfEvents instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdOnDate': instance.createdOnDate,
      'imageUrl': instance.imageUrl,
      'title': instance.title,
      'userId': instance.userId,
      'location': instance.location,
      'eventDate': instance.eventDate,
      'startTime': instance.startTime,
      'endTime': instance.endTime,
    };
