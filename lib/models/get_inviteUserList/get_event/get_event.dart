

import 'package:json_annotation/json_annotation.dart';
part 'get_event.g.dart';

@JsonSerializable(explicitToJson: true)
class ListOfEvents {
  ListOfEvents({
    required this.id,
    required this.createdOnDate,
    required this.imageUrl,
    required this.title,
    required this.userId,
    required this.location,
    required this.eventDate,
    required this.startTime,
    required this.endTime,
  });
  final String? id;
  final int? createdOnDate;
  final String? imageUrl;
  final String? title;
  final String? userId;
  final String? location;
  final String? eventDate;
  final String? startTime;
  final String? endTime;

   factory ListOfEvents.fromJson(Map<String, dynamic> json) =>
      _$ListOfEventsFromJson(json);

  Map<String, dynamic> toJson() => _$ListOfEventsToJson(this);
}
