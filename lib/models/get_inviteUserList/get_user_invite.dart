import 'package:lost_tracking/models/get_inviteUserList/get_invite_events.dart';

import 'package:json_annotation/json_annotation.dart';
part 'get_user_invite.g.dart';

@JsonSerializable(explicitToJson: true)
class GetUserInvite {
  GetUserInvite({
    required this.isSucess,
    required this.data,
    required this.isServerError,
    required this.message,
  });
  final bool? isSucess;
  final Data? data;
  final bool? isServerError;
  final String? message;

  factory GetUserInvite.fromJson(Map<String, dynamic> json) =>
      _$GetUserInviteFromJson(json);

  Map<String, dynamic> toJson() => _$GetUserInviteToJson(this);
}
