
import 'dart:io';


import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:lost_tracking/models/event_model.dart';
import 'package:lost_tracking/models/get_inviteUserList/get_user_invite.dart';
import 'package:lost_tracking/services/http_service.dart';
import 'package:lost_tracking/services/util_service.dart';
import 'package:lost_tracking/utils/service_locator.dart';

class EventProvider extends ChangeNotifier{
  var http=locator<HttpService>();

  // FirebaseStorage _storage = FirebaseStorage.instance;
  final utilService = locator<UtilService>();

  List<EventModel> eventData=[];

   Future<GetUserInvite> getInvitesOfUser() async {
    var response = await http.getUserInvitations();

    if (response.statusCode == 200) {
      print(response.data);
      return GetUserInvite.fromJson(response.data);
    } else {
      throw response.statusCode.toString();
    }
  }

 Future<dynamic> callGetEvent()async{
    try{
      var response= await http.getEvent();

      if(response.statusCode==200){
        var body=response.data["data"]["list"];
        eventData.clear();


        for(var abc in body){
          saveEventData(EventModel.fromJson(abc));
        }
        print('this is length${eventData.length}');
        if(eventData.length<1){
          return "fail";
        }
        else{
          return "done";
        }


      }

    }
    catch(e){
      print(e);
      return "fail";

    }

  }

 Future<dynamic> callPostEvent(Map<String, dynamic> data)async{
    try{
      var response= await http.postEvent(data);

      if(response.statusCode==200){
        var body=response.data["isSuccess"];
        print('this is body$body');
        if(body==true){
          return "success";
        }
        else{
          return "fail";
        }

        // for(var abc in body){
        //   eventData.add(EventModel.fromJson(abc));
        // }
       // print('this is length${eventData.length}');

      }

    }
    catch(e){
      print(e);
      return "fail";

    }

  }

  Future<dynamic> callPostAcceptEvent(String eventID,String userID)async{
    try{
      var response= await http.postAcceptEvent( eventID: 'eventID', userID: 'userID');

      if(response.statusCode==200){
        var body=response.data["isSuccess"];
        print('this is body$body');
        if(body==true){
          return "success";
        }
        else{
          return "fail";
        }

        // for(var abc in body){
        //   eventData.add(EventModel.fromJson(abc));
        // }
        // print('this is length${eventData.length}');

      }

    }
    catch(e){
      print(e);
      return "fail";

    }

  }


  Future<String>  uploadImage(String uid,File image)async{
    try{
      Reference storageReference;
      FirebaseStorage storage = FirebaseStorage.instance;
      //Create a reference to the location you want to upload to in firebase
      storageReference = storage.ref().child("profileImages/$uid");

      final UploadTask uploadTask = storageReference.putFile(image);

      TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);

      // Waits till the file is uploaded then stores the download url
      String url = await taskSnapshot.ref.getDownloadURL();

      print(url);

      return url;
    }
    catch(e){

      return utilService.showToast("Unable to upload Image");
    }


  }

  saveEventData(EventModel value){
    eventData.add(value);
    notifyListeners();

  }

}