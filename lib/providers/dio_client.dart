import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:lost_tracking/models/get_inviteUserList/get_user_invite.dart';
import 'package:lost_tracking/models/invite.dart';

class DioServices extends ChangeNotifier {
  final Dio _dio = Dio(
    BaseOptions(
      baseUrl: "https://big-unison-321920.firebaseapp.com/lost/api/v1/",
    ),
  );

  Future<AcceptInvite> acceptInvite() async {
    Response response = await _dio.post(
      "https://big-unison-321920.firebaseapp.com/lost/api/v1/events/LeQdMH3OaNwJWjhkOx44/user/7LBnRc0rouhvvpke7DOfT1KfO4P2/invite/approve",
      options: Options(
        contentType: 'multipart/form-data',
      ),
      data: {
        "isSucess": "Sucess",
        "data": true,
        "isServerError": false,
        "message": "Invitaion Accepted"
      },
    );
    if (response.statusCode == 200 || response.statusCode!.toInt() <= 205) {
      print(response.data);
      return AcceptInvite.fromJson(response.data);
    } else {
      throw response.statusMessage.toString();
    }
  }

  Future<GetUserInvite> getInvitesOfUser() async {
    Response response = await _dio.get("/invite/user");

    if (response.statusCode == 200) {
      print(response.data);
      return GetUserInvite.fromJson(response.data);
    } else {
      throw response.statusCode.toString();
    }
  }
}
